FROM openjdk:19-slim

LABEL maintainer="Guillaume REMY <guillaume.remy@bs.ch>" \
      version="1.0.0"

# Update the OS and install some tools
RUN apt update \
 && apt install -y curl vim unzip \
 && apt clean

# The dependecies between versions for cordova, java and gradle ist really strict
# And a specific version of cordova will need a specific version of gradle and java.
# See cordova requirements here: https://cordova.apache.org/docs/en/12.x/guide/platforms/android/index.html
# See Java/Gradle compatibility metrix here: https://docs.gradle.org/current/userguide/compatibility.html
# Docker Image Java Version : 19
ENV NODE_VERSION 20
ENV CORDOVA_VERSION 12
ENV GRADLE_VERSION 8.7
ENV ANDROID_VERSION 34
ENV CMDLINE_TOOLS_VERSION 11076708

# Install Node
RUN curl -sL https://deb.nodesource.com/setup_${NODE_VERSION}.x | bash - \
 && apt install -y nodejs && apt clean \
 && npm install -g --upgrade npm

# Install Cordova
RUN npm install -g cordova@${CORDOVA_VERSION} \
 && cordova telemetry off

# Install Gradle
RUN curl -o /tmp/gradle-${GRADLE_VERSION}.zip https://downloads.gradle.org/distributions/gradle-${GRADLE_VERSION}-bin.zip \
 && unzip -d /opt /tmp/gradle-${GRADLE_VERSION}.zip \
 && rm /tmp/gradle-${GRADLE_VERSION}.zip
ENV GRADLE_USER_HOME /opt/gradle-${GRADLE_VERSION}

# Install Android SDK
# https://developer.android.com/studio#command-tools
ENV ANDROID_HOME /usr/lib/android-sdk
ENV PATH ${ANDROID_HOME}/platform-tools:${ANDROID_HOME}/cmdline-tools/latest/bin:${GRADLE_USER_HOME}/bin:${PATH}
RUN apt install -y android-sdk \
 && curl -o /tmp/clt.zip https://dl.google.com/android/repository/commandlinetools-linux-${CMDLINE_TOOLS_VERSION}_latest.zip \
 && unzip -d ${ANDROID_HOME}/cmdline-tools /tmp/clt.zip \
 # The directory MUST be named "/cmdline-tools/latest"
 && mv /usr/lib/android-sdk/cmdline-tools/cmdline-tools /usr/lib/android-sdk/cmdline-tools/latest \
 && apt clean \
 && rm /tmp/clt.zip \
 && yes | sdkmanager --licenses \
 && sdkmanager "platforms;android-${ANDROID_VERSION}" "build-tools;${ANDROID_VERSION}.0.0"

# Do a first dummy build to install all missing dependencies 
# (cordova will download and install them automatically)
# With this, we are also sure that this docker image will work for building android applications
RUN cd /tmp \
 && cordova create geogirafe dev.geogirafe geogirafe \
 && cd geogirafe \
 && cordova platform add android \
 && cordova build android \
 && cd .. \
 && rm -Rf geogirafe

# Configure default directory
RUN mkdir /src
WORKDIR /src
