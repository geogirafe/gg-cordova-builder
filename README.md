
[![Pipeline Status](https://gitlab.com/geogirafe/gg-cordova-builder/badges/main/pipeline.svg)](https://gitlab.com/geogirafe/gg-cordova-builder/-/pipelines)
[![Docker Pulls](https://img.shields.io/docker/pulls/geogirafe/gg-cordova-builder.svg)](https://hub.docker.com/r/geogirafe/cordova-builder/)

This is a image to build android app with cordova.
It's based on:
- Java 19
- Gradle 8.7
- NodeJS 20
- Cordova 12
- Android 34

# How to build

```
docker build -t geogirafe/cordova-builder .
```

# How to run

### Simple

```
docker run -ti --name cordova-builder -v .\dist\app\:/app geogirafe/cordova-builder bash
```
